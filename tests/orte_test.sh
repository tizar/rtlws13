#!/bin/bash
OLDDIR=`pwd`
NUM_MESSAGES=5000;
for MESSAGE_SIZE in 50 100 500 1000 5000
do
	echo ./run_test.sh 1 $MESSAGE_SIZE $NUM_MESSAGES 
	(cd orte && ./run_test.sh 1 $MESSAGE_SIZE $NUM_MESSAGES )
done;
cd $OLDDIR;
