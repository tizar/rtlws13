#!/bin/bash

if [ $# -lt 3 ]
  then
    echo "usage $0 <number of subscriber> <message size> <number of messages>"
    echo "e.g. $0 2 100 1000"
    exit 
fi


DIR=`pwd`
RESULT_DIR=/dev/shm
PUB_PORT=54321
NUM_SUBSCRIBER=$1

MESSAGE_SIZE=$2
NUM_MESSAGE=$3


# run the info repo server
$DDS_ROOT/bin/DCPSInfoRepo   -ORBEndpoint iiop://localhost:12345 &

sleep 1

for i in `seq 1 $NUM_SUBSCRIBER`
do
	# SUB_PORT=$((PUB_PORT+$i))
	echo "running subscriber"
	 (chrt 99 $DIR/subscriber -num $NUM_MESSAGE -s $MESSAGE_SIZE -ns $NUM_SUBSCRIBER -DCPSConfigFile $DIR/../../layer/dds_tcp_conf.ini  > $RESULT_DIR/openddsraw_sub"$i"_"$MESSAGE_SIZE"_"$NUM_MESSAGE".txt)&
	# echo $SUB_PORT
	# pub_opts=$pub_opts" "tcp://127.0.1.1:$SUB_PORT
done
echo "running publisher"
(chrt 98 $DIR/publisher -num $NUM_MESSAGE -s $MESSAGE_SIZE -ns $NUM_SUBSCRIBER -DCPSConfigFile $DIR/../../layer/dds_tcp_conf.ini > $RESULT_DIR/openddsraw_pub_"$MESSAGE_SIZE"_"$NUM_MESSAGE".txt)

killall DCPSInfoRepo subscriber publisher
echo "DONE"
