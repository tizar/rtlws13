Pub/Sub Middleware Manual {#mainpage}
=========================

This middleware consists of APIs that acts as a wrapper for three different publisher/subscriber framework:

1. [Zeromq]
2. [OpenDDS]
3. [ORTE]

The goal is to provide consistent API for publish/subscribe type application regardless of the underlying framework. Thus, the developer is able to choose among the three frameworks above with a minor modification of configuration and code.

##Installation

In order to use of one the this publisher/subscribe middleware, the developer have to install the appropriate framework.

###Zeromq

The source code for [Zeromq] can be downloaded from [the zeromq website](http://zeromq.org/intro:get-the-software). 
In the time of writing this document, the latest stable release is v4.0.4.
However, the middleware was only tested using zeromq v3.2.4.
The steps to install zeromq is described as follows: 

	wget http://download.zeromq.org/zeromq-3.2.4.tar.gz
	tar xf zeromq-3.2.4.tar.gz
	cd zeromq-3.2.4
	./configure
	make
	make install


###OpenDDS

The description of [OpenDDS] from its website:

> OpenDDS is an open source C++ implementation of the Object Management Group (OMG) Data Distribution Service (DDS). OpenDDS also supports Java bindings through JNI and can be included with JBoss (ESB) frameworks by means of a JMS wrapper. OpenDDS leverages the ADAPTIVE Communication Environment (ACE) to provide a cross platform environment.

The latest version of opendds can be downloaded from its [website](http://download.ociweb.com/OpenDDS/). 
This middleware is tested using OpenDDS v3.4.1
The step to install OpenDDS is as follows:

	wget -c http://download.ociweb.com/OpenDDS/OpenDDS-3.4.1.tar.gz
	tar zxvf OpenDDS-3.4.1.tar.gz
	cd DDS
    ./configure
    make 
    make install

There are additional configuration parameters that are available for OpenDDS. 
To find out more about the parameters please refer to its [developers manual](http://download.ociweb.com/OpenDDS/OpenDDS-latest.pdf).

### ORTE

The description of [ORTE] from its website:

> The Open Real-Time Ethernet (ORTE) is an open source implementation of Real-Time Publish-Subscribe (RTPS) communication protocol. RTPS is new application layer protocol targeted to real-time communication area, which is build on the top of standard UDP stack. Since there are many TCP/IP stack implementations under many operating systems and RTPS protocol does not have any other special HW/SW requirements, it should be easily ported to many HW/SW target platforms. Because it uses only UDP protocol, it retains control of timing and reliability.

The latest version of ORTE can be downloaded from [sourceforge](http://sourceforge.net/projects/orte/files/). This middleware is tested using the ORTE v0.3.3.
The steps to install ORTE is as follows:

	tar -zxvf orte-0.3.3.tar.gz
	cd orte-0.3.3
	mkdir build
	cd build 
	../configure
	make
	make install






[Zeromq]: http://zeromq.org/ "zeromq"
[OpenDDS]: http://www.opendds.org/ "opendds"
[ORTE]: http://orte.sourceforge.net/ "orte"

