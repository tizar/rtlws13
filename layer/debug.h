/* 
 * File:   debug.h
 * Author: j
 *
 * Created on May 31, 2012, 1:32 PM
 */

#ifndef DEBUG_H
#define	DEBUG_H

#ifdef DEBUG
#define DBGVAR(X) std::cerr << "var " #X " : " << (X)<<std::endl
#define PDEBUG(X) std::cerr << (X) << std::endl
#else
#define PDEBUG(X)
#define DBGVAR(X) 
#endif

#endif	/* DEBUG_H */

