/** @file zeromq_layer.h
 *  @brief This file contains the zeromq layer concrete class for connection, publisher and subscriber
 *  @version 0.5
*/

#ifndef _ZEROMQ_LAYER_
#define _ZEROMQ_LAYER_ 

#include <string>
#include <vector>
#include <zmq.hpp>
#include "baselayer.h"
#include <pthread.h>

namespace zeromq_layer {
class AConnection;

/**
 * @brief A concrete class for the publisher class for zeromq.
 *
 * This class serves as an API wrapper class for zeromq publisher socket.
 */
class Publisher: public BasePublisher {
	friend class AConnection;
public:
	/**
	 * @brief Default constructor
	 */
	Publisher();

	/**
	 * @brief Send a packet to all subscriber
	 *
	 * This method send a packet using zeromq publisher to all its subscribers.
	 * Zeromq requires binding the publisher socket to a network address.
	 * The network address is defined when the publisher is constructed using the
	 * Connection::create_publisher() method.
	 * The packet contains both the message and the topic.
	 * Thus the length of the packet is the length of message plus the length
	 * of the topic
	 *
	 * @param buf The message
	 * @param message_size The size of the message
	 */
	void send(const char * buf, size_t message_size);
	/**
	 * @brief Destructor
	 */
	virtual ~Publisher();
//	size_t toffset_;
private:
	/**
	 * the publisher zeromq socket
	 */
	zmq::socket_t * socket_;
//	std::string topic_;
	/**
	 * the publisher zeromq network address. The socket is bind to this address
	 */
	std::string address_;

};

/**
 * @brief The concrete class for the subscriber class for zeromq
 *
 * This class serves as an API wrapper class for zeromq subscriber socket.
 */
class Subscriber: public BaseSubscriber {
	friend class AConnection;
public:
	/**
	 * @brief Default constructor
	 *
	 */
	Subscriber();
	/**
	 * @brief Destructor
	 */
	virtual ~Subscriber();
	/**
	 * @brief A blocking receive method
	 *
	 * This method provides a blocking message received.
	 * The current thread is blocked until the subscriber socket received a message
	 * with the subscribed topic.
	 * This method is not preferred. Please look at register_callback() instead.
	 *
	 * @note zeromq raw packet from the publisher contains the topic.
	 * This method remove the topic, thus returning only the message
	 *
	 * @param buf The received message. This method expect that he array has been preallocated
	 * @return the size of the message received
	 */
	int recv(char * buf);
	/**
	 * @brief Register a function that is called whenever the subscriber receive a message
	 *
	 * This method registered a user defined function that is called when a message received
	 * by the zeromq socket.
	 * The subscriber object has and internal thread that listens for incoming packets.
	 * The thread trigger the callback function.
	 * It provides a non-blocking mechanism to receive a packet.
	 *
	 * @param fun The user defined callback function
	 */
	void register_callback(void (*fun)(char*, size_t));
	/**
	 * @brief a helper function that listen on a socket
	 *
	 * This helper function ran on a separate thread when the subscriber is created.
	 * It blocks an waits for an incoming message.
	 */
	void * process(void);
private:
	/**
	 * The zeromq subscriber socket
	 */
	zmq::socket_t * socket_;
//	std::string topic_;
	/**
	 * The publisher address that the subscriber connects to
	 */
	std::string address_;
//	size_t toffset_;
	/**
	 * A thread that waits for incoming packet
	 */
	pthread_t listener_;
	/**
	 * The user defined callback function
	 * @param buf Received message
	 * @param size Message size
	 */
	void (*recv_callback_)(char* buf, size_t size);
	int stop_;

};

/**
 * @brief The concrete class for the connection class for zeromq
 */
class AConnection: public BaseAConnection {
	friend class Publisher;
	friend class Subscriber;
public:
	/**
	 * @brief Default constructor
	 */
	AConnection();

	/**
	 * Initialize the zeromq context
	 * @param argc The number of arguments. Set to 0
	 * @param argv The array of arguments. Set to NULL
	 * @return
	 */
	int init(int argc, char * argv[]);
	/**
	 * @brief Default destructor
	 */
	virtual ~AConnection();

	/**
	 * @brief Create a zeromq subscriber.
	 *
	 * This method create a zeromq subscriber using the current context.
	 * The subscriber listens for the topic defined and connects to a publisher
	 * on the network address given
	 *
	 * @param topic Topic filter
	 * @param address The publisher network address
	 * @return the subscriber
	 */
	Subscriber *create_subscriber(std::string topic, std::string address);

	/**
	 * @brief Create a zeromq publisher.
	 *
	 * This method create a zeromq publisher using the current context.
	 *
	 * @param topic Topic filter
	 * @param address The publisher network address
	 * @return the publisher
	 */
	Publisher *create_publisher(std::string topic, std::string address);
private:
	zmq::context_t *context_;
};
}
#endif
