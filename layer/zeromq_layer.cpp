#include "zeromq_layer.h"
#include "zmq.h"
#include <iostream>
#include "timestamp.h"

using namespace std;


Publisher::Publisher(){
	toffset_ = 0;
}

Publisher * AConnection::create_publisher(string topic,string address){
	Publisher * pub = new Publisher();
	pub->topic_ = topic;
	pub->address_ = address;
	pub->toffset_ = pub->topic_.length();
    pub->socket_ =  new zmq::socket_t(*(context_), ZMQ_PUB);
    pub->socket_->bind(address.c_str());
    // std::cerr  << "[layer init] topic "<< topic_ << "\t"  << topic_.length() << "\t" << toffset_<< std::endl;
    return pub; 
}

Publisher::~Publisher(){}

void Publisher::send( const char * buf,size_t message_size){
	int res;

	// send the message, add the topic to the message
	// std::cerr  << "[layer send] topic "<< topic_ << "\t"  << topic_.length() << "\t" << toffset_<< std::endl;
	zmq::message_t msg(message_size+toffset_);
	memcpy(msg.data(), topic_.c_str(), toffset_);
    memcpy(static_cast<char *>(msg.data())+toffset_, buf, message_size);

    // std::cerr <<"[layer send] " << msg.size() << "\t" << topic_ << "\t"<< (char*)msg.data() << "\t" << buf << std::endl;
	res = socket_->send(msg);
	if (res < 0) std::cerr << "Error: " << res << std::endl;
}

Subscriber::Subscriber(){
	toffset_ = 0;
	stop_ = 0;
}

static void * process_helper (void *context){
	return ((Subscriber *)context)->process();
}
	
void * Subscriber::process(void){
	// TODO fix this, findout how to pass the message size from main
	size_t message_size = 50000;
	char * buf;
    buf = (char*)malloc(message_size * sizeof(char));
	while (!stop_){
        message_size = recv(buf);
        recv_callback_(buf,message_size);
    }

    return NULL;
}

Subscriber * AConnection::create_subscriber(string topic,string address){
	Subscriber * sub = new Subscriber();
	sub->topic_ = topic;
	sub->toffset_ = sub->topic_.length();
	sub->address_ = address;
    sub->socket_ =  new zmq::socket_t(*(context_), ZMQ_SUB);
    sub->socket_->connect(address.c_str());
	sub->socket_->setsockopt(ZMQ_SUBSCRIBE,sub->topic_.c_str(), sub->topic_.length());
	//create listener
	pthread_create (&sub->listener_, NULL, process_helper, sub);
	return sub;
}	

Subscriber::~Subscriber(){}

void Subscriber::register_callback(void (*fun)(char* buf, size_t size)){
    recv_callback_ =  fun;
}


int Subscriber::recv(char * buf){
	// buf must be allocated outside;
	
	zmq::message_t msg;
	socket_->recv(&msg);
	size_t message_size = msg.size()-toffset_;
	memcpy(buf, static_cast<char *>(msg.data())+toffset_, message_size);
	// std::cerr <<"[layer recv] " << msg.size() << "\t" << topic_ << "\t"<< (char*)msg.data() << "\t" << buf << std::endl;
	return msg.size()-topic_.length();
}

AConnection::AConnection(){}

int AConnection::init(int argc, char * argv[]){
	context_ = new zmq::context_t();	
	return 0;
}

zmq::context_t * AConnection::get_context(){
	return context_;
}

AConnection::~AConnection(){}






