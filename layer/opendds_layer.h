/** @file opendds_layer.h
 *  @brief This file contains the opendds layer concrete class for connection, publisher and subscriber
 *  @version 0.5
*/

#ifndef _OPENDDS_LAYER_
#define _OPENDDS_LAYER_

#include <string>
#include <vector>
#include <dds/DdsDcpsInfoUtilsC.h>
#include <dds/DCPS/Service_Participant.h>
#include <dds/DCPS/PublisherImpl.h>
#include <dds/DCPS/SubscriberImpl.h>
#include <dds/DCPS/transport/tcp/TcpInst.h>
#include <dds/DCPS/Marked_Default_Qos.h>
#include <dds/DCPS/DomainParticipantImpl.h>
#include <dds/DCPS/BuiltInTopicUtils.h>
#include <dds/DCPS/WaitSet.h>
#include "MessageC.h"
#include "MessageTypeSupportImpl.h"
#include "RawBufferListenerImpl.h"
#include <boost/thread/mutex.hpp>
#include "baselayer.h"

namespace opendds_layer {
class Publisher;
class Subscriber;

/**
 * The concrete class for the connection class for zeromq
 */
class AConnection: public BaseAConnection {
	friend class Publisher;
	friend class Subscriber;
public:
	/**
	 * @brief Default constructor
	 */
	AConnection();
	/**
	 * @brief Default destructor
	 */
	virtual ~AConnection();
	/**
	 * @brief Initialize opendds
	 *
	 * This method initialize opendds DomainParticipantFactory and participant.
	 * It loads the dds config file.
	 *
	 * @param argc The number of arguments. Set to 0
	 * @param argv The array of arguments. Set to NULL
	 * @return
	 */
    int init(int argc, char * argv[]);
    /**
	 * @brief Create an opendds subscriber.
	 *
	 * This method create an opendds subscriber using the opendds participant,
	 * create a listener for the message and register it to the subscriber.
	 *
	 * @param topic Topic filter
	 * @param address The publisher network address
	 * @return the subscriber
	 */
    Subscriber *create_subscriber(std::string topic, std::string address);
    /**
	 * @brief Create an opendds publisher.
	 *
	 * This method create an opendds publisher using the opendds participant,
	 * and create opendds data writer.
	 *
	 * @param topic Topic filter
	 * @param address The publisher network address
	 * @return the publisher
	 */
    Publisher *create_publisher(std::string topic, std::string address);
private:
	/**
	 * Factory for creating Domain Participants
	 */
	::DDS::DomainParticipantFactory_var dpf_;
	/**
	 * The Domain Participant reference
	 */
	::DDS::DomainParticipant_var participant_;
	/**
	 * Instance handle for publishing (one per key published)
	 */
	::DDS::InstanceHandle_t handle_;

};

/**
 * @brief A concrete class for the publisher class for opendds.
 *
 * This class serves as an API wrapper class for opendds publisher object.
 */
class Publisher : public BasePublisher {
	friend class AConnection;
public:
	/**
	 * @brief Default constructor
	 */
	Publisher();
	/**
	 * @brief Default destructor
	 */
	virtual ~Publisher();
	/**
	 * @brief Send a packet to all subscriber
	 *
	 * This method send a packet using opendds publisher to all its subscribers.
	 * Opendds does not require explicit address for its publisher
	 *
	 * @param buf The message
	 * @param message_size The size of the message
	 */
	void send(const char * buf, size_t message_size);
private:
	/**
	 *  The publisher reference
	 */
	::DDS::Publisher_var publisher_;
	/**
	 * The Topic that will be published and subscribed to
	 */
	::DDS::Topic_var rawbuffer_topic_;
	/**
	 * The Data Writer reference (does the actual publishing)
	 */
	::DDS::DataWriter_var rawbuffer_base_dw_;
	/**
	 * Implementation of the Data writer
	 */
	Message::RawBufferDataWriter_var rawbuffer_dw_;
	/**
	 * Type support
	 */
	Message::RawBufferTypeSupport_var rawbuffer_ts_;
};

/**
 * @brief A concrete class for the subscriber class for opendds.
 *
 * This class serves as an API wrapper class for opendds subscriber object.
 */
class Subscriber : public BaseSubscriber{
	friend class AConnection;
	friend class RawBufferListenerImpl;
public:
	/**
	 * @brief Default constructor
	 */
	Subscriber();
	/**
	 * @brief Default destructor
	 */
	virtual ~Subscriber();
	/**
	 * @brief A blocking receive method
	 *
	 * This method provides a blocking message received.
	 * This is not implemented yet for opendds
	 *
	 * @warning Do not use this method. Look at register_callback()
	 *
	 * @param buf The received message. This method expect that he array has been preallocated
	 * @return the size of the message received
	 */
	int recv(char * buf);
	/**
	 * @brief Register a function that is called whenever the subscriber receive a message
	 *
	 * This method registered a user defined function that is called when a message received.
	 * The callback function is called from the helper class RawBufferListenerImpl
	 *
	 * @param fun The user defined callback function
	 */
	void register_callback(void (*fun)(char*,size_t));
private:
//	std::string topic_;
	/**
	 * The subscriber reference
	 */
	::DDS::Subscriber_var subscriber_;
	/**
	 * The Topic that will be published and subscribed to
	 */
	::DDS::Topic_var rawbuffer_topic_;
	/**
	 * Listener for alerting that data has been published
	 */
	::DDS::DataReaderListener_var rawbuffer_listener_;
	/**
	 * Data Reader for receiving the published data
	 */
	::DDS::DataReader_var rawbuffer_base_dr_;

	Message::RawBufferDataReader_var rawbuffer_dr_;
	Message::RawBufferTypeSupport_var rawbuffer_ts_; // this maybe removed?
	void (* recv_callback_)(char*,size_t);
	boost::mutex *recv_mutex_;
	int is_new_message_arrived_;
	char * message_;
	size_t message_size_;
};
}
#endif
