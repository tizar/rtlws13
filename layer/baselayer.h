/** @file baselayer.h
 *  @brief This file contains the abstract classes for connection, publisher and subscriber
 *  @version 0.5
*/

class BaseAConnection;

/**
 * This class is the abstract class for the publisher.
 */
class BasePublisher{
public:
	/**
	 * @brief Send a packet to all subscribers.
	 *
	 * The method send() only transmit packet to
	 * all subscribers that subscribe to the publisher topic.
	 *
	 * @param buf the buffer to be sent
	 * @param message_size the size of the message
	 */
	virtual void send(const char * buf,size_t message_size)=0;
	/**
	 * @brief Virtual destructor
	 */
	virtual ~BasePublisher(){};
protected:
	/**
	 * length of the topic
	 */
	size_t toffset_;

	/**
	 * the publisher topic
	 */
	std::string topic_;
};

/**
 * This class is the abstract class for the subscriber
 */
class BaseSubscriber{
public:
	/**
	 * @brief Virtual destructor
	 */
	virtual ~BaseSubscriber(){};
	/**
	 * @brief Receive a packet from the publisher
	 *
	 * This method is a blocking method.
	 * The current thread that call this function will be blocked until it receive a packet.
	 * The recv() method only unblock if it receive a packet with the subscribed topic.
	 * The method is usually not used due to its blocking nature, thus the callback method
	 * is preferred.
	 * Please look at register_callback()
	 *
	 * @param buf an array that contains the received packet
	 * @return the packet size
	 */
	virtual int recv(char * buf)=0;
	/**
	 * @brief Register a callback function that is called when the subscriber receive a packet
	 *
	 * This method register a callback function that is called whenever the subscriber received a new message.
	 * The arrival of a packet will trigger the fun() method.
	 * This is a way to have a non-blocking mechanism to receive a message
	 *
	 * @param fun is the user defined callback function
	 */
	virtual void register_callback(void (*fun)(char*,size_t))=0;
protected:
	/**
	 * length of the topic
	 */
	size_t toffset_;

	/**
	 * the publisher topic
	 */
	std::string topic_;

};

/**
 * This class handle to connection initialization and setup
 */
class BaseAConnection{
public:
	/**
	 * @brief Initialize the connection for publisher/subscriber
	 *
	 * @param argc the number of input arguments
	 * @param argv the array of arguments
	 * @return The status of the connection, 0 means the connection is successful and fail otherwise.
	 */
    virtual int init(int argc, char * argv[])=0;
    /**
     * @brief The virtual destructor
     */
    virtual ~BaseAConnection(){};
    /**
     * @brief create a subscriber in the current connection
     *
     * This method register a subscriber in the connection that listen
     * to a particular topic.
     *
     * @param topic The topic filter
     * @param address The publisher network address. Set to NULL if the address is not required
     * @return The subscriber object
     */
    BaseSubscriber *create_subscriber(std::string topic, std::string address);
    /**
 	 * @brief create a publisher in the current connection
	 *
	 * This method register a publisher in the connection that publish message
	 * for a particular topic.
	 *
	 * @param topic The topic filter
	 * @param address The publisher network address. Set to NULL if the address is not required
	 * @return The subscriber object
	 */
    BasePublisher *create_publisher(std::string topic, std::string address);
private:

};
