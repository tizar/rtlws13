#include "orte_layer.h"
#include <iostream>
#include "timestamp.h"
#include <assert.h>
#include <string.h>
#define DEBUG
#include "debug.h"

using namespace std;


AConnection::AConnection(){
	domain_ = NULL;
}

int AConnection::init(int argc, char * argv[]){
	ORTEInit();
  	// ORTEVerbositySetOptions("ALL.10");
  	domain_=ORTEDomainAppCreate(ORTE_DEFAULT_DOMAIN,NULL,NULL,ORTE_FALSE);
  	if (!domain_) {
    	std::cerr << "ORTEDomainAppCreate failed!\n";
		return 1;
	}
	return 0;
}


AConnection::~AConnection(){
	ORTEDomainAppDestroy(domain_);
}


Publisher::Publisher(){
}

// char              instance2Recv[64];

Publisher * AConnection::create_publisher(string topic,string address){
	Publisher * pub = new Publisher();
	pub->topic_ = topic;
	pub->address_ = address;
	pub->toffset_ = pub->topic_.length();
	pub->typename_ = pub->topic_+"_type";
	ORTETypeRegisterAdd(domain_,pub->typename_.c_str(),NULL,NULL,NULL,sizeof(pub->sendbuffer_));
	NTPTIME_BUILD(pub->persistence_,3); // this topic is valid for 3 seconds
	NTPTIME_BUILD(pub->delay_,1);       // callback function called every 1 second
	pub->publisher_=ORTEPublicationCreate(
	  domain_,                 	// domain
	  pub->topic_.c_str(), 		// topic
	  pub->typename_.c_str(),   // typename
	  &(pub->sendbuffer_),     	// output buffer
	  &(pub->persistence_),     				// persistance of publication
	  1,
	  NULL, 					//send callback, null because we use ORTEPublicationSend. instead
	  NULL,
	  &(pub->delay_)
	  );					// delay of publication
    return pub; 
}


void recv_callback_helper(const ORTERecvInfo *info,void *vinstance, void *recvCallBackParam) {
  char *instance=(char*)vinstance;
  Subscriber * sub = (Subscriber*) recvCallBackParam;
  switch (info->status) {
    case NEW_DATA:
    	sub->recv_callback_(instance,sizeof(instance));
      // printf("%s\n",instance);
      	break;
    case DEADLINE:
      	std::cerr << "deadline occurred\n";
      	break;
  }
}



Subscriber * AConnection::create_subscriber(string topic,string address){
	Subscriber * sub = new Subscriber();
	sub->topic_ = topic;
	sub->toffset_ = sub->topic_.length();
	sub->address_ = address;
	sub->typename_ = sub->topic_+"_type";

	ORTETypeRegisterAdd(domain_,sub->typename_.c_str(),NULL,NULL,NULL,sizeof(sub->recvbuffer_));
	NTPTIME_BUILD(sub->deadline_,10); // deadline of read at 10 seconds
	NTPTIME_BUILD(sub->minimum_separation_,0); 
	sub->subscriber_=ORTESubscriptionCreate(
	   domain_,
	   sub->subscription_mode_,
	   sub->subscription_type_,
	   sub->topic_.c_str(),
	   sub->typename_.c_str(),
	   &(sub->recvbuffer_),
	   &(sub->deadline_),
	   &(sub->minimum_separation_),
	   recv_callback_helper,
	   sub,
	   IPADDRESS_INVALID);

	return sub;
}	


Publisher::~Publisher(){}

void Publisher::send( const char * buf,size_t message_size){
	assert(message_size<=Publisher::MAXBUFFER);
	memcpy(sendbuffer_, buf, message_size);
	ORTEPublicationSend(publisher_);
}

Subscriber::Subscriber(){
	toffset_ = 0;
	subscription_mode_ = IMMEDIATE;
	subscription_type_ = BEST_EFFORTS;
}


Subscriber::~Subscriber(){}

void Subscriber::register_callback(void (*fun)(char* buf, size_t size)){
    recv_callback_ =  fun;
}


int Subscriber::recv(char * buf){
	// TODO blocking read do not use this function
	return 0;
}








